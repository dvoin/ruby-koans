
# "name","amount","category","date","description"

# Expenses - parse the expenses.csv file and print some meaningful statistics

#     total spent per user
#     total spent per user/per category
#     total spent per year/month

require 'csv'

csv = CSV.read('expenses.csv', :headers => true, :col_sep => ',')

#puts csv['name'].class , csv[1].class
#puts csv['name'], csv[1], csv[1][0]

# puts "---------------------------"
# csv.each do |row|
# 	puts row['date']
# end
# puts "---------------------------"	

hash_per_user = Hash.new(0) 
hash_per_user_per_category = Hash.new(0)
hash_per_year_per_month = Hash.new(0)

csv.each do |row|
	hash_per_user[row['name']] += row['amount'].to_f
end

csv.each do |row|
	hash_per_user_per_category[row['name']+ ' - ' +row['category']] += row['amount'].to_f
end

csv.each do |row|
	hash_per_year_per_month[row['date'].split('.')[2] + '/' + row['date'].split('.')[1]] += row['amount'].to_f
end

#puts hash_per_user.inspect + "\n\n"
#puts hash_per_user_per_category.inspect + "\n\n"
#puts hash_per_year_per_month.inspect + "\n\n"

puts "\n"
puts "Expenses per user: "
hash_per_user.each_pair { |name, val| puts "#{name} -> #{val}" }
puts "\n"
puts "Expenses per user/per category: "
hash_per_user_per_category.each_pair { |name, val| puts "#{name} -> #{val}" }
puts "\n"
puts "Expenses per year/month: "
hash_per_year_per_month.each_pair { |name, val| puts "#{name} -> #{val}" }

